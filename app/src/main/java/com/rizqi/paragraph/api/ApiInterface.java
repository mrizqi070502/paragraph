package com.rizqi.paragraph.api;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("register")
    Call<Map<String,Object>> register(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("login")
    Call<Map<String,Object>> login(@FieldMap Map<String, String> user);

}
