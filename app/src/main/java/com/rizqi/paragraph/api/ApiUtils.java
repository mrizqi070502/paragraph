package com.rizqi.paragraph.api;

public class ApiUtils {

    private ApiUtils(){
    }

    public static final String API_URL = "http://fauzan-dev.divistant.com/api/auth/";

    public static ApiInterface getApiInterface(){
        return RetrofitClient.getClient(API_URL).create(ApiInterface.class);
    }

}
