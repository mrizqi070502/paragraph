package com.rizqi.paragraph.model;

import java.util.List;

public class Post {
    private String id;
    private String title;
    private String image;
    private List<String> tags;
    private String body;
    private String userId;
    private Long views;
    private Long hours;
    private String createdDate;

    public Post() {
    }

    public Post(String id, String title, String image, List<String> tags, String body, String userId, Long views, Long hours, String createdDate) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.tags = tags;
        this.body = body;
        this.userId = userId;
        this.views = views;
        this.hours = hours;
        this.createdDate = createdDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public Long getHours() {
        return hours;
    }

    public void setHours(Long hours) {
        this.hours = hours;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", tags=" + tags +
                ", body='" + body + '\'' +
                ", userId='" + userId + '\'' +
                ", views=" + views +
                ", hours=" + hours +
                ", createdDate='" + createdDate + '\'' +
                '}';
    }
}
