package com.rizqi.paragraph.model;

public class User {
    private String id;
    private String fullname;
    private String email;
    private String profpictUrl;

    public User(){}

    public User(String id, String fullname, String email, String profpictUrl) {
        this.id = id;
        this.fullname = fullname;
        this.email = email;
        this.profpictUrl = profpictUrl;
    }

    public String getId() {return id;}

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfpictUrl() {
        return profpictUrl;
    }

    public void setProfpictUrl(String profpictUrl) {
        this.profpictUrl = profpictUrl;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", fullname='" + fullname + '\'' +
                ", email='" + email + '\'' +
                ", profilePicture='" + profpictUrl + '\'' +
                '}';
    }
}
