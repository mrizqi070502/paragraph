package com.rizqi.paragraph;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

    private static String LOGIN_STATUS = "login_status";
    private static String USER_ID = "user_id";
    private static String TOKEN = "access_token";

    private static SharedPreferences getSharedPreference(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
    public static void setLoggedInStatus(Context context, boolean status){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putBoolean(LOGIN_STATUS,status);
        editor.apply();
    }

    public static boolean getLoggedInStatus(Context context){
        return getSharedPreference(context).getBoolean(LOGIN_STATUS,false);
    }

    public static void setUserId(Context context, String id){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(USER_ID,id);
        editor.apply();
    }

    public static String getUserId(Context context){
        return getSharedPreference(context).getString(USER_ID,"");
    }

    public static void setToken(Context context, String token){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(TOKEN,token);
        editor.apply();
    }

    public static String getToken(Context context){
        return getSharedPreference(context).getString(TOKEN,"");
    }

}
