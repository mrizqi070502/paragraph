package com.rizqi.paragraph.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.rizqi.paragraph.databinding.ItemNewpostBinding;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.view.ReadPostActivity;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class NewPostAdapter extends RecyclerView.Adapter<NewPostAdapter.ViewHolder>{

    List<Post> newPosts;

    public NewPostAdapter(List<Post> newPosts) {
        this.newPosts = newPosts;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        ItemNewpostBinding binding = ItemNewpostBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        Post post = newPosts.get(position);
        FirebaseDatabase.getInstance().getReference("users")
                .child(post.getUserId()).child("fullname")
                .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                String name =dataSnapshot.getValue(String.class);
                holder.binding.textviewBywho.setText("by "+name);
            }
        });
        Picasso.with(holder.itemView.getContext()).load(post.getImage()).into(holder.binding.imageviewPicture);
        holder.binding.textviewPosttitle.setText(post.getTitle());
        holder.binding.textviewHour.setText(post.getHours().longValue()+" hours ago");
        holder.binding.imageviewPicture.setOnClickListener(v->{goToDetailPost(v,post);});
        holder.binding.textviewPosttitle.setOnClickListener(v->{goToDetailPost(v,post);});
        holder.binding.linearlayoutInfo.setOnClickListener(v->{goToDetailPost(v,post);});
    }

    @Override
    public int getItemCount() {
        return newPosts.size();
    }

    public void goToDetailPost(View view, Post post){
        Intent intent = new Intent(view.getContext(), ReadPostActivity.class);
        intent.putExtra("postId",post.getId());
        view.getContext().startActivity(intent);
    }

    public void setNewPosts(List<Post> newPosts) {
        this.newPosts = newPosts;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ItemNewpostBinding binding;

        public ViewHolder(@NonNull @NotNull ItemNewpostBinding mBinding) {
            super(mBinding.getRoot());
            this.binding = mBinding;
        }
    }
}
