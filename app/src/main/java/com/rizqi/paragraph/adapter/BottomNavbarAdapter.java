package com.rizqi.paragraph.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.rizqi.paragraph.view.HomeFragment;
import com.rizqi.paragraph.view.ProfileFragment;
import com.rizqi.paragraph.view.SearchFragment;

import org.jetbrains.annotations.NotNull;

public class BottomNavbarAdapter extends FragmentStatePagerAdapter {

    private FragmentManager fragmentManager;

    public BottomNavbarAdapter(@NonNull @NotNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        this.fragmentManager = fm;
    }

    @NonNull
    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new HomeFragment();
            case 1:
                return new SearchFragment(this.fragmentManager);
            case 2:
                return new ProfileFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
