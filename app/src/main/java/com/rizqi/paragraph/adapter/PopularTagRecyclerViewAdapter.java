package com.rizqi.paragraph.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.rizqi.paragraph.databinding.ItemNewpostBinding;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.view.ReadPostActivity;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PopularTagRecyclerViewAdapter extends RecyclerView.Adapter<PopularTagRecyclerViewAdapter.ViewHolder>{

    private List<Post> popularTagList;

    public PopularTagRecyclerViewAdapter(List<Post> popularTagList) {
        this.popularTagList = popularTagList;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        ItemNewpostBinding binding = ItemNewpostBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        ItemNewpostBinding binding = holder.binding;
        Post post = popularTagList.get(position);
        FirebaseDatabase.getInstance().getReference("users")
                .child(post.getUserId()).child("fullname")
                .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                binding.textviewBywho.setText("by "+dataSnapshot.getValue(String.class));
            }
        });
        Picasso.with(holder.itemView.getContext()).load(post.getImage()).into(binding.imageviewPicture);
        binding.textviewPosttitle.setText(post.getTitle());
        binding.textviewHour.setText(post.getViews().intValue()+" views");
        holder.binding.imageviewPicture.setOnClickListener(v->{goToDetailPost(v,post);});
        holder.binding.textviewPosttitle.setOnClickListener(v->{goToDetailPost(v,post);});
        holder.binding.linearlayoutInfo.setOnClickListener(v->{goToDetailPost(v,post);});
    }

    @Override
    public int getItemCount() {
        return popularTagList.size();
    }

    public void goToDetailPost(View view, Post post){
        Intent intent = new Intent(view.getContext(), ReadPostActivity.class);
        intent.putExtra("postId",post.getId());
        view.getContext().startActivity(intent);
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ItemNewpostBinding binding;

        public ViewHolder(@NonNull @NotNull ItemNewpostBinding sBinding) {
            super(sBinding.getRoot());
            this.binding = sBinding;
        }
    }
}
