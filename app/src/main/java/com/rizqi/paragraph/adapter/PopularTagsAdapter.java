package com.rizqi.paragraph.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class PopularTagsAdapter extends FragmentStateAdapter {

    private List<String> popularTags = new ArrayList<>();
    private List<Fragment> fragments = new ArrayList<>();


    public void addFragment(Fragment fragment, String title){
        popularTags.add(title);
        fragments.add(fragment);
    }

    public PopularTagsAdapter(@NonNull @NotNull FragmentManager fragmentManager, @NonNull @NotNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @NotNull
    @Override
    public Fragment createFragment(int position) {
        return fragments.get(position);
    }

    @Override
    public int getItemCount() {
        return fragments.size();
    }
}
