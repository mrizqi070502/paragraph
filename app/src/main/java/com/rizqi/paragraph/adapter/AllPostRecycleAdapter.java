package com.rizqi.paragraph.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.annotations.NotNull;
import com.rizqi.paragraph.databinding.ItemPostDefaultBinding;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.view.PreviewPostActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AllPostRecycleAdapter extends RecyclerView.Adapter<AllPostRecycleAdapter.ViewHolder>{

    private List<Post> allYourPost;

    public AllPostRecycleAdapter(List<Post> allYourPostList) {
        this.allYourPost = allYourPostList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPostDefaultBinding binding = ItemPostDefaultBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AllPostRecycleAdapter.ViewHolder holder, int position) {
        Post post = allYourPost.get(position);
        ItemPostDefaultBinding binding = holder.binding;
        Picasso.with(holder.itemView.getContext()).load(post.getImage()).into(binding.postImage);
        binding.titleTextview.setText(post.getTitle());
        binding.viewsTextview.setText(post.getViews().intValue()+" views");
        binding.bywhoTextview.setVisibility(View.GONE);
        binding.hourTextview.setText(post.getHours().intValue()+" hours ago");
        binding.tagchipLinearlayout.setVisibility(View.GONE);
        ViewGroup.LayoutParams params = binding.postImage.getLayoutParams();
        params.height = 200;
        params.width = 200;
        binding.postImage.setLayoutParams(params);
        holder.binding.postImage.setOnClickListener(v-> startReadPost(v,post));
        holder.binding.linearlayoutInfo.setOnClickListener(v-> startReadPost(v,post));

    }

    @Override
    public int getItemCount() {
        return allYourPost.size();
    }

    public void setAllYourPost(List<Post> allYourPost) {
        this.allYourPost = allYourPost;
        notifyDataSetChanged();
    }

    public void startReadPost(View view, Post post){
        Intent intent = new Intent(view.getContext(), PreviewPostActivity.class);
        intent.putExtra("postId",post.getId());
        view.getContext().startActivity(intent);

    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ItemPostDefaultBinding binding;

        public ViewHolder(@NonNull @NotNull ItemPostDefaultBinding sBinding) {
            super(sBinding.getRoot());
            this.binding = sBinding;
        }
    }
}
