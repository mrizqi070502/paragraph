package com.rizqi.paragraph.adapter;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

public class ScrollerCustom extends Scroller {

    private double mscrollFactor = 1;

    public ScrollerCustom(Context context) {
        super(context);
    }

    public ScrollerCustom(Context context, Interpolator interpolator) {
        super(context, interpolator);
    }

    public ScrollerCustom(Context context, Interpolator interpolator, boolean flywheel) {
        super(context, interpolator, flywheel);
    }
    public void setScrollDurationFactor(double scrollFactor){
        mscrollFactor = scrollFactor;
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        super.startScroll(startX, startY, dx, dy, (int) (duration*mscrollFactor));
    }
}
