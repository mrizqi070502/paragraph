package com.rizqi.paragraph.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.rizqi.paragraph.R;


public class DeleteBottomSheetDialog extends BottomSheetDialogFragment {

    private BottomSheetListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_dialog, container, false);
        MaterialButton positiveButton = view.findViewById(R.id.button_positive);
        MaterialButton negativeButton = view.findViewById(R.id.button_negative);

        negativeButton.setOnClickListener(v->dismiss());
        positiveButton.setOnClickListener(v->{
            dismiss();
            mListener.onPositiveButtonClicked();
        });

        return view;
    }
    public interface BottomSheetListener{
        void onPositiveButtonClicked();
        void onNegativeButtonClicked();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            mListener = (BottomSheetListener) context;
        }catch (ClassCastException e){}
    }
    @Override
    public int getTheme() {
        return R.style.CustomBottomSheetDialog;
    }
}