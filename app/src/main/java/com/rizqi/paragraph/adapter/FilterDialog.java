package com.rizqi.paragraph.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.rizqi.paragraph.R;
import com.rizqi.paragraph.databinding.ItemFilterDialogBinding;

import java.util.List;

public class FilterDialog {
    public ItemFilterDialogBinding binding;

    private String tag = "", byWho = "";
    private List<String> tags;

    public FilterDialog(ItemFilterDialogBinding binding, Context context, List<String> tags) {
        this.binding = binding;
        this.tags = tags;
        setAutoComplete(context, tags);
    }

    public void setAutoComplete(Context context, List<String> tags){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.autocomplete_tags, tags);
        this.binding.autocompleteTag.setAdapter(adapter);
    }

    public String getTag() {
        return binding.autocompleteTag.getText().toString();
    }

    public void setTag(String tag){
        this.tag = tag;
    }

    public void setTagList(List<String> tags, Context context){
        this.tags = tags;
        setAutoComplete(context, tags);
    }

    public String getByWho() {
        return binding.edittextWho.getText().toString();
    }

    public Long getViewsFrom() {
        Long n = -1L;
        try {
            n = Long.parseLong(binding.edittextFromview.getText().toString());
        }catch (NumberFormatException e){}
        return n;
    }

    public Long getViewsTo() {
        Long n = -1L;
        try {
            n = Long.parseLong(binding.edittextToview.getText().toString());
        }catch (NumberFormatException e){}
        return n;
    }

    public Long getHoursFrom() {
        Long n = -1L;
        try {
            n = Long.parseLong(binding.edittextFromhour.getText().toString());
        }catch (NumberFormatException e){}
        return n;
    }

    public Long getHoursTo() {
        Long n = -1L;
        try {
            n = Long.parseLong(binding.edittextTohour.getText().toString());
        }catch (NumberFormatException e){}
        return n;
    }

    public void clearForm(){
        binding.autocompleteTag.setText("");
        binding.edittextWho.setText("");
        binding.edittextFromview.setText("");
        binding.edittextToview.setText("");
        binding.edittextFromhour.setText("");
        binding.edittextTohour.setText("");
    }

    public boolean isValid(){
        boolean valid = false;
        valid |= getTag().trim().length() > 0 ? true : false;
        valid |= getByWho().trim().length() > 0 ? true : false;
        valid |= ((getViewsFrom() >= 0) && (getViewsTo() >= 0)) ? true : false;
        valid |= ((getHoursFrom() >= 0) && (getHoursTo() >= 0)) ? true : false;
        return valid;
    }

}
