package com.rizqi.paragraph.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;

import com.asksira.loopingviewpager.LoopingPagerAdapter;
import com.google.android.material.imageview.ShapeableImageView;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.view.ReadPostActivity;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class LoopingTrendSlideAdapter extends LoopingPagerAdapter<Post> {

    List<Post> trendingPosts;

    public LoopingTrendSlideAdapter(@NotNull List<Post> itemList, boolean isInfinite) {
        super(itemList, isInfinite);
        trendingPosts = itemList;
    }

    @Override
    protected void bindView(@NotNull View view, int i, int i1) {
        Post post = trendingPosts.get(i);
        TextView title = view.findViewById(R.id.trendpost_textview_title);
        AppCompatTextView readmore= view.findViewById(R.id.trendpost_compattextview_readmore);
        ShapeableImageView imageView = view.findViewById(R.id.trendpost_imageview_1);
        title.setText(post.getTitle());
        Picasso.with(view.getContext()).load(post.getImage()).into(imageView);
        imageView.setOnClickListener(v -> {goToDetailPost(view, post);});
        title.setOnClickListener(v -> {goToDetailPost(view, post);});
        readmore.setOnClickListener(v -> {goToDetailPost(view,post);});
    }

    public void goToDetailPost(View view, Post post){
        Intent intent = new Intent(view.getContext(), ReadPostActivity.class);
        intent.putExtra("postId",post.getId());
        view.getContext().startActivity(intent);
    }

    @NotNull
    @Override
    protected View inflateView(int i, @NotNull ViewGroup viewGroup, int i1) {
        return LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_trendingpost_viewpager2, viewGroup, false);
    }
}
