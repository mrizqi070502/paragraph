package com.rizqi.paragraph.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.chip.Chip;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.databinding.ItemFilteredSearchBinding;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.view.CreatePostActivity;
import com.rizqi.paragraph.view.ReadPostActivity;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SearchFilteredRecyclerViewAdapter extends RecyclerView.Adapter<SearchFilteredRecyclerViewAdapter.ViewHolder>{

    private List<Post> searchResults;

    public SearchFilteredRecyclerViewAdapter(List<Post> searchResults) {
        this.searchResults = searchResults;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        ItemFilteredSearchBinding binding = ItemFilteredSearchBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        Post post = searchResults.get(position);
        ItemFilteredSearchBinding binding = holder.binding;
        Picasso.with(holder.itemView.getContext()).load(post.getImage()).into(binding.filteredImageview);
        binding.textviewTitle.setText(post.getTitle());
        binding.textviewViews.setText(post.getViews().intValue()+" views");
        FirebaseDatabase.getInstance().getReference("users")
                .child(post.getUserId()).child("fullname")
                .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                binding.textviewBywho.setText("by "+formatName(dataSnapshot.getValue(String.class)));
            }
        });
        binding.textviewHour.setText(post.getHours().intValue()+" hours ago");
        List<String> tags = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            tags = post.getTags().stream().distinct().collect(Collectors.toList());
        }
        binding.linearlyoutChip.removeAllViews();
        for (String tag:
                tags) {
            @SuppressLint("ResourceType")
            Chip chip = new Chip(holder.itemView.getContext());
            chip.setText(tag);
            chip.setChipMinHeight(90);
            chip.setCloseIconVisible(true);
            chip.setTextColor(holder.itemView.getResources().getColor(R.color.grey_1));
            chip.setChipBackgroundColor(ColorStateList.valueOf(holder.itemView.getResources().getColor(R.color.grey_5)));
            Typeface typeface = ResourcesCompat.getFont(holder.itemView.getContext(), R.font.poppins);
            chip.setTypeface(typeface);
            chip.setTextSize(10);
            chip.setTextEndPadding(0);
            chip.setCloseIconStartPadding(0);
            int colorInt = holder.itemView.getResources().getColor(R.color.grey_1);
            chip.setCloseIconVisible(false);
            binding.linearlyoutChip.addView(chip);
        }

        holder.binding.filteredImageview.setOnClickListener(v->{goToDetailPost(v,post);});
        holder.binding.linearlayoutInfo.setOnClickListener(v->{goToDetailPost(v,post);});

    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    public void setSearchResults(List<Post> posts){
        this.searchResults.clear();
        this.searchResults = posts;
        notifyDataSetChanged();
    }

    private String formatName(String name){
        String username = name.replace("Muhammad","M").replace("Mohammad","M");
        String[] split = username.split(" ");
        String finalName = "";
        int i = 0;
        if (split[0].equals("M")){
            finalName = "M "+split[1];
            i = 2;
        }else {
            finalName = split[0];
            i = 1;
        }
        for (; i < split.length; i++) {
            finalName += " "+split[i].toUpperCase().charAt(0);
        }
        return finalName;
    }

    public void goToDetailPost(View view, Post post){
        Intent intent = new Intent(view.getContext(), ReadPostActivity.class);
        intent.putExtra("postId",post.getId());
        view.getContext().startActivity(intent);
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ItemFilteredSearchBinding binding;

        public ViewHolder(@NonNull @NotNull ItemFilteredSearchBinding sBinding) {
            super(sBinding.getRoot());
            this.binding = sBinding;
        }
    }
}
