package com.rizqi.paragraph.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.chip.Chip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.databinding.ActivityCreatePostBinding;
import com.rizqi.paragraph.model.Post;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CreatePostActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 1;
    private ActivityCreatePostBinding binding;
    private List<String> defaultTags;
    private List<String> resultTags;
    private List<String> baseTags;
    private List<String> newTags;
    private ArrayAdapter<String> adapter;
    private Uri mImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCreatePostBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        defaultTags = new ArrayList<>();
        resultTags = new ArrayList<>();
        baseTags = new ArrayList<>();
        newTags = new ArrayList<>();

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadTags();
        adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.autocomplete_tags, defaultTags);
        binding.autocompleteTag.setAdapter(adapter);
        binding.buttonNewTag.setOnClickListener(v->{
            createChip(binding.autocompleteTag.getText().toString());
        });
        binding.autocompleteTag.setOnItemClickListener((parent, view, position, id) -> createChip(adapter.getItem(position)));
        binding.buttonChooseimage.setOnClickListener(v->openImageDirectory());
        binding.compattextviewRemoveimage.setOnClickListener(v->{
            binding.imageviewShowimage.setImageResource(0);
            mImageUri = null;
        });
        binding.buttonCancel.setOnClickListener(v->onBackPressed());
        binding.buttonBack.setOnClickListener(v->onBackPressed());
        binding.buttonUpload.setOnClickListener(v->uploadPost());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            mImageUri = data.getData();
            Picasso.with(CreatePostActivity.this).load(mImageUri).into(binding.imageviewShowimage);
        }
    }

    private void uploadPost(){
        if (mImageUri != null){
            binding.progressbarUploadimage.setVisibility(View.VISIBLE);
            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            String postId = FirebaseDatabase.getInstance().getReference("posts").push().getKey();
            String imageId = userId+"_"+postId;
            StorageReference reference = FirebaseStorage.getInstance().getReference("posts").child(imageId);
            reference.putFile(mImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            reference.getDownloadUrl()
                                    .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            String imageUrl = uri.toString();
                                            String title = binding.edittextTitle.getText().toString();
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                resultTags = resultTags.stream().distinct().collect(Collectors.toList());
                                            }
                                            String body = binding.edittextBody.getText().toString();
                                            Date now = Calendar.getInstance().getTime();
                                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                            String createdDate = dateFormat.format(now);
                                            Post post = new Post(postId,title, imageUrl, resultTags, body, userId, 0L, 0L, createdDate);
                                            FirebaseDatabase.getInstance().getReference("posts").child(postId)
                                                    .setValue(post)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void unused) {
                                                    DatabaseReference tagsReference = FirebaseDatabase.getInstance().getReference("tags");
                                                    tagsReference.get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                                                        @Override
                                                        public void onSuccess(DataSnapshot dataSnapshot) {
                                                            int size = ((List<String>)dataSnapshot.getValue()).size();
                                                            for (String s:
                                                                    newTags) {
                                                                tagsReference.child(String.valueOf(size)).setValue(s);
                                                                size++;
                                                                Toast.makeText(CreatePostActivity.this, "Upload successful", Toast.LENGTH_SHORT).show();
                                                                binding.progressbarUploadimage.setVisibility(View.GONE);
                                                                finish();
                                                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                                                            }
                                                        }
                                                    })
                                                            .addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@NonNull Exception e) {
                                                                    Toast.makeText(CreatePostActivity.this, "Upload successful", Toast.LENGTH_SHORT).show();
                                                                    binding.progressbarUploadimage.setVisibility(View.GONE);
                                                                    finish();
                                                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                                                                }
                                                            });
                                                    binding.progressbarUploadimage.setVisibility(View.GONE);
                                                    finish();
                                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Toast.makeText(CreatePostActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                    binding.progressbarUploadimage.setVisibility(View.GONE);
                                                }
                                            });
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(CreatePostActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                            binding.progressbarUploadimage.setVisibility(View.GONE);
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(CreatePostActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            binding.progressbarUploadimage.setVisibility(View.GONE);
                        }
                    });
        }else {
            Toast.makeText(this, "Please, choose an image!", Toast.LENGTH_SHORT).show();
        }
    }

    private void openImageDirectory(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void loadTags(){
        FirebaseDatabase.getInstance()
                .getReference("tags")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        for (DataSnapshot snapshot:
                             task.getResult().getChildren()) {
                            defaultTags.add(snapshot.getValue(String.class));
                            baseTags.add(snapshot.getValue(String.class));
                            adapter.notifyDataSetChanged();
                        }
                    }else {
                        System.out.println("Failed to load tags");
                    }
                });
    }

    private void createChip(String text){
        if (!resultTags.contains(text)){
            Chip chip = new Chip(CreatePostActivity.this);
            chip.setText(text);
            chip.setChipMinHeight(90);
            chip.setCloseIconVisible(true);
            chip.setTextColor(getResources().getColor(R.color.grey_1));
            chip.setChipBackgroundColor(ColorStateList.valueOf(getResources().getColor(R.color.grey_5)));
            Typeface typeface = ResourcesCompat.getFont(getApplicationContext(), R.font.poppins);
            chip.setTypeface(typeface);
            chip.setTextSize(10);
            chip.setTextEndPadding(0);
            chip.setCloseIconStartPadding(0);
            int colorInt = getResources().getColor(R.color.grey_1);
            chip.setCloseIconTint(ColorStateList.valueOf(colorInt));
            chip.setOnCloseIconClickListener(v -> {
                binding.tagchipLinearlayout.removeView(v);
                defaultTags.add(text);
                newTags.remove(text);
                resultTags.remove(text);
                System.out.println("DEFAULT : "+Arrays.toString(defaultTags.toArray()));
                System.out.println("NEW : "+Arrays.toString(newTags.toArray()));
                System.out.println("RESULT : "+Arrays.toString(resultTags.toArray()));
                adapter.notifyDataSetChanged();
            });
            if (!baseTags.contains(text)){
                newTags.add(text);
            }
            binding.tagchipLinearlayout.addView(chip);
            defaultTags.remove(text);
            resultTags.add(text);
            System.out.println("DEFAULT : "+Arrays.toString(defaultTags.toArray()));
            System.out.println("NEW : "+Arrays.toString(newTags.toArray()));
            System.out.println("RESULT : "+Arrays.toString(resultTags.toArray()));
            adapter.remove(text);
            adapter.notifyDataSetChanged();
        }
        binding.autocompleteTag.setText("");
    }
}