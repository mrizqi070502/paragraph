package com.rizqi.paragraph.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.annotations.NotNull;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.databinding.ActivityForgotPasswordBinding;

public class ForgotPasswordActivity extends AppCompatActivity{

    private ActivityForgotPasswordBinding binding;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityForgotPasswordBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

//        config
        binding.compatbuttonSendemail.setOnClickListener(v->{
            resetPassword();
        });

    }

    private void resetPassword(){
        String email = binding.edittextEmail.getText().toString();
        if (email.isEmpty()){
            binding.inputLayoutEmail.setError("Email is required");
            binding.inputLayoutEmail.setErrorEnabled(true);
            binding.inputLayoutEmail.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            binding.inputLayoutEmail.setError("Invalid email");
            binding.inputLayoutEmail.setErrorEnabled(true);
            binding.inputLayoutEmail.requestFocus();
            return;
        }
        binding.forgotpasswordProgressbar.setVisibility(View.VISIBLE);
        auth = FirebaseAuth.getInstance();
        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<Void> task) {
                        if (task.isSuccessful()){
                            binding.forgotpasswordProgressbar.setVisibility(View.GONE);
                            Toast.makeText(ForgotPasswordActivity.this, "Check your email to reset password!",Toast.LENGTH_SHORT).show();
                        }else {
                            binding.forgotpasswordProgressbar.setVisibility(View.GONE);
                            Toast.makeText(ForgotPasswordActivity.this, "Try again! Something wrong happened!",Toast.LENGTH_SHORT).show();
                            finish();
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        binding.forgotpasswordProgressbar.setVisibility(View.GONE);
                        Toast.makeText(ForgotPasswordActivity.this, e.getMessage(),Toast.LENGTH_SHORT).show();
                        finish();
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                });
    }

}