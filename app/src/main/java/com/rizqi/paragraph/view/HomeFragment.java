package com.rizqi.paragraph.view;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.rizqi.paragraph.adapter.LoopingTrendSlideAdapter;
import com.rizqi.paragraph.adapter.NewPostAdapter;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.service.PostService;
import com.rizqi.paragraph.databinding.FragmentHomeBinding;

import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    //    vars
    private FragmentHomeBinding binding;
    private LoopingTrendSlideAdapter loopingTrendSlideAdapter;
    private NewPostAdapter newPostAdapter;

    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        //  config
        setupPost();
        binding.pageindicatorDots.setCount(3);
        binding.viewPager2.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
                binding.pageindicatorDots.setSelection(position-1);
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }

    @Override
    public void onPause() {
        binding.viewPager2.pauseAutoScroll();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.viewPager2.resumeAutoScroll();
    }

    private void setupPost(){
        FirebaseDatabase.getInstance().getReference("posts")
                .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                    @Override
                    public void onSuccess(DataSnapshot dataSnapshot) {
                        List<Post> allPost = new ArrayList<>();
                        for (DataSnapshot snap:
                                dataSnapshot.getChildren()) {
                            Post post = snap.getValue(Post.class);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                LocalDateTime date = LocalDateTime.parse(post.getCreatedDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                                LocalDateTime dateNow = LocalDateTime.now();
                                Duration duration = Duration.between(date, dateNow);
                                post.setHours(duration.toHours());
                            }
                            allPost.add(post);
                        }
                        loopingTrendSlideAdapter = new LoopingTrendSlideAdapter(PostService.getTrendingPosts(allPost),true);
                        binding.viewPager2.setAdapter(loopingTrendSlideAdapter);
                        newPostAdapter = new NewPostAdapter(PostService.getNewPosts(allPost));
                        binding.recyclerviewNewposts.setLayoutManager(new LinearLayoutManager(getContext()));
                        binding.recyclerviewNewposts.setAdapter(newPostAdapter);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(getContext(), "Failed to load trending posts! "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

}