package com.rizqi.paragraph.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.NotNull;
import com.rizqi.paragraph.Preferences;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.api.ApiUtils;
import com.rizqi.paragraph.databinding.ActivityLoginBinding;
import com.rizqi.paragraph.model.User;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding binding;
    private FirebaseAuth mAuth;
    private GoogleSignInClient googleSignInClient;
    private static int RC_SIGN_IN = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

//        config
        createRequest();
        binding.emailEdittext.addTextChangedListener(getTextWatcher());
        binding.passwordEdittext.addTextChangedListener(getTextWatcher());
        binding.toregisterCompattextview.setOnClickListener(v -> {
            startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.loginButton.setOnClickListener(v -> {
            if (isFormValid()){
                loginUser();

            }
        });
        binding.forgotpasswordCompattextview.setOnClickListener(v->{
            startActivity(new Intent(getApplicationContext(),ForgotPasswordActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.googleImagebutton.setOnClickListener(v->openSignIn());

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                Log.w(TAG, "Google sign in failed", e);
                Toast.makeText(getApplicationContext(), e.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isFormValid(){
        boolean isValid = true;
        String email = binding.emailEdittext.getText().toString();
//                validating email
        Pattern pattern = Pattern.compile("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b");
        Matcher matcher = pattern.matcher(email);
        boolean isEmailValid = matcher.find();
        isValid &= isEmailValid;
        if (!isEmailValid){
            binding.emailInputlayout.requestFocus();
            binding.emailInputlayout.setErrorEnabled(true);
            binding.emailInputlayout.setError("Email not valid!");
        }else {
            binding.emailInputlayout.setErrorEnabled(false);
            binding.emailInputlayout.setError("");
        }
        isValid &= binding.passwordEdittext.getText().toString().length() >= 8;
        if (binding.passwordEdittext.getText().toString().length() < 8){
            binding.passwordInputlayout.requestFocus();
            binding.passwordInputlayout.setErrorEnabled(true);
            binding.passwordInputlayout.setError("Password not valid!");
        }else {
            binding.passwordInputlayout.setErrorEnabled(false);
            binding.passwordInputlayout.setError("");
        }
        return isValid;
    }

    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            String id = user.getUid();
                            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                            firebaseDatabase.getReference("users").child(id)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot snapshot) {
                                            String defaultProfPictUrl = user.getPhotoUrl().toString();
                                            User userModel = new User(id,user.getDisplayName(), user.getEmail(),defaultProfPictUrl);
                                            firebaseDatabase.getReference("users").child(id).setValue(userModel);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError error) {
                                            Toast.makeText(LoginActivity.this, error.getMessage(),Toast.LENGTH_SHORT).show();
                                        }
                                    });
                            binding.loginProgressbar.setVisibility(View.GONE);
                            Preferences.setLoggedInStatus(LoginActivity.this, true);
                            apiLogin();
                            Intent intent = new Intent(getApplicationContext(), BottomNavbarActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Login failed! Try again",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void createRequest(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(this,gso);
    }

    private void openSignIn(){
        binding.loginProgressbar.setVisibility(View.VISIBLE);
        Intent intent = googleSignInClient.getSignInIntent();
        startActivityForResult(intent, RC_SIGN_IN);
    }

    private TextWatcher getTextWatcher(){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String email = binding.emailEdittext.getText().toString();
//                validating email
                Pattern pattern = Pattern.compile("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b");
                Matcher matcher = pattern.matcher(email);
                boolean isEmailValid = matcher.find();
                if (binding.passwordEdittext.getText().toString().length() >= 8 && isEmailValid){
                    binding.loginButton.setBackgroundResource(R.drawable.login_register_valid_shape);
                    binding.loginButton.setTextColor(Color.WHITE);
                }else{
                    binding.loginButton.setBackgroundResource(R.drawable.login_register_invalid_shape);
                    binding.loginButton.setTextColor(Color.BLACK);
                }
            }
        };
    }

    private void loginUser(){
        String email = binding.emailEdittext.getText().toString();
        String password = binding.passwordEdittext.getText().toString();
        binding.loginProgressbar.setVisibility(View.VISIBLE);
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            binding.loginProgressbar.setVisibility(View.GONE);
                            Preferences.setLoggedInStatus(getApplicationContext(),true);
                            Intent intent = new Intent(getApplicationContext(), BottomNavbarActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }else {
                            Toast.makeText(getApplicationContext(), "Failed to login! Try again",Toast.LENGTH_SHORT).show();
                            binding.loginProgressbar.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void apiLogin(){
        String email = binding.emailEdittext.getText().toString();
        String password = binding.passwordEdittext.getText().toString();
        HashMap<String, String> user = new HashMap<>();
        user.put("email",email);
        user.put("password",password);
        Call<Map<String, Object>> call = ApiUtils.getApiInterface().login(user);
        call.enqueue(new Callback<Map<String, Object>>() {
            @Override
            public void onResponse(Call<Map<String, Object>> call, Response<Map<String, Object>> response) {
                if(response.isSuccessful()){
                    System.out.println("Login Successfull");
                    Preferences.setToken(LoginActivity.this, response.body().get("access_token").toString());
                }
            }

            @Override
            public void onFailure(Call<Map<String, Object>> call, Throwable throwable) {
                System.out.println("ERROR : "+throwable);
            }
        });
    }


}