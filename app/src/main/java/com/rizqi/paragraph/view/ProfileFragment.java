package com.rizqi.paragraph.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rizqi.paragraph.Preferences;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.databinding.FragmentProfileBinding;
import com.rizqi.paragraph.model.User;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;
    private Uri mImageUri;
    private User user;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        setUserProfile();
        binding.linearlayoutLogout.setOnClickListener(v-> logOut());
        binding.iseditCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.iseditCheckbox.isChecked()){
                    binding.edittextName.setEnabled(true);
                    binding.edittextName.requestFocus();
                }else {
                    binding.edittextName.setEnabled(false);
                    updateName();
                }
            }
        });
        binding.linearlayoutAbout.setOnClickListener(v->startActivity(new Intent(getContext(), AboutActivity.class)));
        binding.linearlayoutTermofuse.setOnClickListener(v->startActivity(new Intent(getContext(), TermOfUseActivity.class)));
        binding.profilePicture.setOnClickListener(v->{
            Intent intent = new Intent(getContext(), ProfilePictureActivity.class);
            ActivityOptionsCompat compat = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), binding.profilePicture, ViewCompat.getTransitionName(binding.profilePicture));
            startActivity(intent, compat.toBundle());
        });
        binding.cardviewOrganizepost.setOnClickListener(v->{
            startActivity(new Intent(getContext(),OrganizePostActivity.class));

        });
        binding.imagebuttonOrganizepost.setOnClickListener(v->{
            startActivity(new Intent(getContext(),OrganizePostActivity.class));

        });
    }

//    @Override
//    public void startActivityForResult(Intent intent, int requestCode) {
//        super.startActivityForResult(intent, requestCode);
//        if (requestCode == PICK_IMAGE_REQUEST && requestCode == RESULT_OK && intent != null && intent.getData() != null){
//            mImageUri = intent.getData();
//            Picasso.with(getContext()).load(mImageUri).into(binding.profilePicture);
//        }
//    }

    private void updateName(){
        String newName = binding.edittextName.getText().toString();
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase.getInstance().getReference("users").child(userId)
                .child("fullname").setValue(newName)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(getContext(), "Name changed successfully!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(getContext(), "Failed to change the name! "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void logOut(){
        GoogleSignInOptions gso = new GoogleSignInOptions.
                Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                build();
        GoogleSignInClient googleSignInClient= GoogleSignIn.getClient(getContext(),gso);
        googleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<Void> task) {
                FirebaseAuth.getInstance().signOut();
                Preferences.setLoggedInStatus(getContext(), false);
                Intent intent = new Intent(getContext(), OnBoardingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private void setUserProfile() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String userId = firebaseUser.getUid();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users/");
        databaseReference.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                user = snapshot.getValue(User.class);
                if (mImageUri == null){
                    Picasso.with(getContext()).load(user.getProfpictUrl()).into(binding.profilePicture);
                }
                else {
                    Picasso.with(getContext()).load(mImageUri).into(binding.profilePicture);
                }
                binding.edittextName.setText(user.getFullname());
                binding.edittextEmail.setText(user.getEmail());
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
                Toast.makeText(getContext(),"Failed to load profile! Try again!",Toast.LENGTH_SHORT);
            }
        });
    }

}