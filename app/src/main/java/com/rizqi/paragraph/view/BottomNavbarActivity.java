package com.rizqi.paragraph.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationBarView;

import com.rizqi.paragraph.R;
import com.rizqi.paragraph.adapter.BottomNavbarAdapter;
import com.rizqi.paragraph.databinding.ActivityBottomNavbarBinding;

import org.jetbrains.annotations.NotNull;

public class BottomNavbarActivity extends AppCompatActivity {

    private ActivityBottomNavbarBinding binding;
    private BottomNavbarAdapter navbarAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBottomNavbarBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        navbarAdapter = new BottomNavbarAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        binding.bottomnavbarViewpager.setAdapter(navbarAdapter);
        binding.bottomnavbar.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_bottomnavbar_home:
                        binding.bottomnavbarViewpager.setCurrentItem(0);
                        break;
                    case R.id.menu_bottomnavbar_search:
                        binding.bottomnavbarViewpager.setCurrentItem(1);
                        break;
                    case R.id.menu_bottomnavbar_profile:
                        binding.bottomnavbarViewpager.setCurrentItem(2);
                        break;
                }
                return true;
            }
        });
        binding.bottomnavbarViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                binding.bottomnavbar.getMenu().getItem(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        binding.bottomnavbarViewpager.setScrollDurationFactor(1.5);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
    }

}