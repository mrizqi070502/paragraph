package com.rizqi.paragraph.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.rizqi.paragraph.adapter.PopularTagRecyclerViewAdapter;
import com.rizqi.paragraph.databinding.FragmentPopularTagsBinding;
import com.rizqi.paragraph.model.Post;

import java.util.List;

public class PopularTagsFragment extends Fragment {

    private FragmentPopularTagsBinding binding;
    private List<Post> popularTag;
    private PopularTagRecyclerViewAdapter adapter;

    public PopularTagsFragment(List<Post> popularTag) {
        this.popularTag = popularTag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentPopularTagsBinding.inflate(inflater,container,false);
        adapter = new PopularTagRecyclerViewAdapter(popularTag);
        binding.recyclerviewPopulartag.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerviewPopulartag.setAdapter(adapter);
        return binding.getRoot();
    }
}