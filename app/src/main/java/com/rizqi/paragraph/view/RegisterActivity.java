package com.rizqi.paragraph.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.storage.FirebaseStorage;
import com.rizqi.paragraph.Preferences;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.api.ApiUtils;
import com.rizqi.paragraph.databinding.ActivityRegisterBinding;
import com.rizqi.paragraph.model.User;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private ActivityRegisterBinding binding;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

//        config
        binding.fullnameEdittext.addTextChangedListener(getTextWatcher());
        binding.emailEdittext.addTextChangedListener(getTextWatcher());
        binding.passwordEdittext.addTextChangedListener(getTextWatcher());
        binding.confirmpasswordEdittext.addTextChangedListener(getTextWatcher());
        binding.tologinCompattextview.setOnClickListener(v -> {
            finish();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.registerButton.setOnClickListener(v -> {
            if (isFormValid()){
                registerUser();

            }
        });

    }

    private boolean isFormValid(){
        boolean isValid = true;
        String email = binding.emailEdittext.getText().toString();
//                validating email
        Pattern pattern = Pattern.compile("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b");
        Matcher matcher = pattern.matcher(email);
        boolean isEmailValid = matcher.find();
        isValid &= isEmailValid;
        if (!isEmailValid){
            binding.emailInputlayout.requestFocus();
            binding.emailInputlayout.setErrorEnabled(true);
            binding.emailInputlayout.setError("Email not valid!");
        }else {
            binding.emailInputlayout.setErrorEnabled(false);
            binding.emailInputlayout.setError("");
        }
        isValid &= binding.fullnameEdittext.getText().toString().trim().length() > 0;
        if (binding.fullnameEdittext.getText().toString().trim().length() < 1){
            binding.fullnameInputlayout.requestFocus();
            binding.fullnameInputlayout.setErrorEnabled(true);
            binding.fullnameInputlayout.setError("Please! Fill the fullname");
        }else {
            binding.fullnameInputlayout.setErrorEnabled(false);
            binding.fullnameInputlayout.setError("");
        }
        isValid &= binding.passwordEdittext.getText().toString().length() >= 8;
        if (binding.passwordEdittext.getText().toString().length() < 8){
            binding.passwordInputlayout.requestFocus();
            binding.passwordInputlayout.setErrorEnabled(true);
            binding.passwordInputlayout.setError("Password not valid!");
        }else {
            binding.passwordInputlayout.setErrorEnabled(false);
            binding.passwordInputlayout.setError("");
        }
        isValid &= binding.passwordEdittext.getText().toString().equals(binding.confirmpasswordEdittext.getText().toString());
        if (!binding.passwordEdittext.getText().toString().equals(binding.confirmpasswordEdittext.getText().toString())){
            binding.confirmpasswordLayout.requestFocus();
            binding.confirmpasswordLayout.setErrorEnabled(true);
            binding.confirmpasswordLayout.setError("Password and the confirm password are not same!");
        }else {
            binding.confirmpasswordLayout.setErrorEnabled(false);
            binding.confirmpasswordLayout.setError("");
        }
        return isValid;
    }

    private TextWatcher getTextWatcher(){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String username = binding.fullnameEdittext.getText().toString();
                String email = binding.emailEdittext.getText().toString();
                String password = binding.passwordEdittext.getText().toString();
                String confirmPassword = binding.confirmpasswordEdittext.getText().toString();

//                validating email
                Pattern pattern = Pattern.compile("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b");
                Matcher matcher = pattern.matcher(email);
                boolean isEmailValid = matcher.find();

//                validating password
                boolean  isPasswordValid = password.length() >= 8 && confirmPassword.length() >= 8 ? password.equals(confirmPassword) : false;
                if (username.trim().length() > 0 && isEmailValid && isPasswordValid){
                    binding.registerButton.setBackgroundResource(R.drawable.login_register_valid_shape);
                    binding.registerButton.setTextColor(Color.WHITE);
                }else{
                    binding.registerButton.setBackgroundResource(R.drawable.login_register_invalid_shape);
                    binding.registerButton.setTextColor(Color.BLACK);
                }
            }
        };
    }

    private void registerUser(){
        String username = binding.fullnameEdittext.getText().toString();
        String email = binding.emailEdittext.getText().toString();
        String password = binding.passwordEdittext.getText().toString();
        binding.registerProgressbar.setVisibility(View.VISIBLE);
        mAuth = FirebaseAuth.getInstance();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseStorage.getInstance()
                                    .getReference("users")
                                    .child("default_user.png")
                                    .getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                            User user = new User(id,username, email, uri.toString());
                                            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                                            firebaseDatabase.getReference("users").child(id)
                                                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull @NotNull Task<Void> task) {
                                                    if (task.isSuccessful()){
                                                        Toast.makeText(getApplicationContext(), "User has been registered successfully",Toast.LENGTH_SHORT).show();
                                                        binding.registerProgressbar.setVisibility(View.GONE);
                                                        Preferences.setUserId(getApplicationContext(),user.getId());
                                                        Preferences.setLoggedInStatus(getApplicationContext(), true);
                                                        apiRegister();
                                                        Intent intent = new Intent(getApplicationContext(), BottomNavbarActivity.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        startActivity(intent);
                                                    }else {
                                                        Toast.makeText(getApplicationContext(), "Failed to register! Try again",Toast.LENGTH_SHORT).show();
                                                        binding.registerProgressbar.setVisibility(View.GONE);
                                                    }
                                                }
                                            });
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(getApplicationContext(), e.getMessage(),Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }else {
                            Toast.makeText(getApplicationContext(), "Failed to register! Try again",Toast.LENGTH_SHORT).show();
                            binding.registerProgressbar.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void apiRegister(){
        String name = binding.fullnameEdittext.getText().toString();
        String email = binding.emailEdittext.getText().toString();
        String password = binding.passwordEdittext.getText().toString();
        String confirm_password = binding.confirmpasswordEdittext.getText().toString();
        HashMap<String, String> user = new HashMap<>();
        user.put("name",name);
        user.put("email",email);
        user.put("password",password);
        user.put("password_confirmation",confirm_password);
        Call<Map<String, Object>> call = ApiUtils.getApiInterface().register(user);
        call.enqueue(new Callback<Map<String, Object>>() {
            @Override
            public void onResponse(Call<Map<String, Object>> call, Response<Map<String, Object>> response) {
                if(response.isSuccessful()){
                    System.out.println("Register Successfull");
                }
            }

            @Override
            public void onFailure(Call<Map<String, Object>> call, Throwable throwable) {
                System.out.println("ERROR: "+throwable.getMessage());
            }
        });
    }

}