package com.rizqi.paragraph.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.rizqi.paragraph.R;
import com.rizqi.paragraph.databinding.ActivityTermOfUseBinding;

public class TermOfUseActivity extends AppCompatActivity {

    ActivityTermOfUseBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTermOfUseBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.buttonBack.setOnClickListener(v->finish());

    }
}