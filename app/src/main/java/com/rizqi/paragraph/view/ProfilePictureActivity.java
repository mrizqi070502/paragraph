package com.rizqi.paragraph.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rizqi.paragraph.databinding.ActivityProfilePictureBinding;
import com.rizqi.paragraph.model.User;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

public class ProfilePictureActivity extends AppCompatActivity {

    private ActivityProfilePictureBinding binding;
    private User user;
    private Uri mImageUri;
    private String defaultUrl;
    private static final int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfilePictureBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

    @Override
    protected void onStart() {
        super.onStart();
        setUserProfile();
        binding.compattextviewChangeprofpict.setOnClickListener(v -> {
            openImageDirectory();
            binding.linearlayoutChangeprofpict.setVisibility(View.INVISIBLE);
            binding.linearlayoutSave.setVisibility(View.VISIBLE);
        });
        binding.compattextviewRemove.setOnClickListener(v -> {
            binding.linearlayoutChangeprofpict.setVisibility(View.INVISIBLE);
            binding.linearlayoutSave.setVisibility(View.VISIBLE);
            setDefaultPicture();
        });
        binding.view1.setOnClickListener(v->finishAfterTransition());
        binding.buttonDiscard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Picasso.with(ProfilePictureActivity.this).load(user.getProfpictUrl()).into(binding.profileImage);
                binding.linearlayoutChangeprofpict.setVisibility(View.VISIBLE);
                binding.linearlayoutSave.setVisibility(View.GONE);
            }
        });
        binding.buttonSave.setOnClickListener(v -> {
            if (mImageUri != null) {
                updateNewPicture();
            } else {
                Toast.makeText(ProfilePictureActivity.this, "Profile picture can't be empty!", Toast.LENGTH_SHORT).show();
                binding.linearlayoutChangeprofpict.setVisibility(View.VISIBLE);
                binding.linearlayoutSave.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            mImageUri = data.getData();
            Picasso.with(ProfilePictureActivity.this).load(mImageUri).into(binding.profileImage);
        }
    }

    private void updateNewPicture() {
        StorageReference reference = FirebaseStorage.getInstance().getReference("users").child(user.getId());
        reference.putFile(mImageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        reference.getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                        FirebaseDatabase.getInstance().getReference("users").child(userId)
                                                .child("profpictUrl").setValue(uri.toString())
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void unused) {
                                                        Toast.makeText(ProfilePictureActivity.this, "Profile picture changed successfully!", Toast.LENGTH_SHORT).show();
                                                        binding.linearlayoutChangeprofpict.setVisibility(View.VISIBLE);
                                                        binding.linearlayoutSave.setVisibility(View.GONE);
                                                        finishAfterTransition();
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull @NotNull Exception e) {
                                                        Toast.makeText(ProfilePictureActivity.this, "Failed to change profile picture! " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                                        binding.linearlayoutChangeprofpict.setVisibility(View.VISIBLE);
                                                        binding.linearlayoutSave.setVisibility(View.GONE);
                                                    }
                                                });
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull @NotNull Exception e) {
                                        Toast.makeText(ProfilePictureActivity.this, "Failed to change profile picture " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                        binding.linearlayoutChangeprofpict.setVisibility(View.VISIBLE);
                                        binding.linearlayoutSave.setVisibility(View.GONE);
                                    }
                                });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(ProfilePictureActivity.this, "Failed to change profile picture " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        binding.linearlayoutChangeprofpict.setVisibility(View.VISIBLE);
                        binding.linearlayoutSave.setVisibility(View.GONE);
                    }
                });
    }

    private void setDefaultPicture() {
        FirebaseStorage.getInstance().getReference("users").child("default_user.png")
                .getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                defaultUrl = uri.toString();
                Picasso.with(ProfilePictureActivity.this).load(defaultUrl).into(binding.profileImage);
                binding.buttonSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        FirebaseDatabase.getInstance().getReference("users").child(userId)
                                .child("profpictUrl").setValue(defaultUrl)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        Toast.makeText(ProfilePictureActivity.this, "Profile picture changed successfully!", Toast.LENGTH_SHORT).show();
                                        binding.linearlayoutChangeprofpict.setVisibility(View.VISIBLE);
                                        binding.linearlayoutSave.setVisibility(View.GONE);
                                        finishAfterTransition();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull @NotNull Exception e) {
                                        Toast.makeText(ProfilePictureActivity.this, "Failed to change profile picture! " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                        binding.linearlayoutChangeprofpict.setVisibility(View.VISIBLE);
                                        binding.linearlayoutSave.setVisibility(View.GONE);
                                    }
                                });
                    }
                });

                binding.buttonDiscard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(ProfilePictureActivity.this).load(user.getProfpictUrl()).into(binding.profileImage);
                        binding.linearlayoutChangeprofpict.setVisibility(View.VISIBLE);
                        binding.linearlayoutSave.setVisibility(View.GONE);
                    }
                });
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(ProfilePictureActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void openImageDirectory() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    private void setUserProfile() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        String userId = firebaseUser.getUid();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users");
        databaseReference.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                user = snapshot.getValue(User.class);
                User ur = snapshot.getValue(User.class);
                if (mImageUri == null) {
                    Picasso.with(ProfilePictureActivity.this).load(ur.getProfpictUrl()).into(binding.profileImage);
                } else {
                    Picasso.with(ProfilePictureActivity.this).load(mImageUri).into(binding.profileImage);
                }
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
                Toast.makeText(ProfilePictureActivity.this, "Failed to load profile! Try again!", Toast.LENGTH_SHORT);
            }
        });
    }
}