package com.rizqi.paragraph.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.chip.Chip;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.adapter.DeleteBottomSheetDialog;
import com.rizqi.paragraph.databinding.ActivityPreviewPostBinding;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.model.User;
import com.squareup.picasso.Picasso;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class PreviewPostActivity extends AppCompatActivity implements DeleteBottomSheetDialog.BottomSheetListener{

    ActivityPreviewPostBinding binding;
    Post post;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPreviewPostBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }
    @Override
    protected void onStart() {
        super.onStart();
        post = new Post();
        user = new User();
        binding.readPost.buttonBack.setOnClickListener(v -> onBackPressed());
        loadPost();
        binding.buttonDelete.setOnClickListener(v->{
            DeleteBottomSheetDialog bottomSheetDialog = new DeleteBottomSheetDialog();
            bottomSheetDialog.show(getSupportFragmentManager(),"Delete");
        });
        binding.buttonEdit.setOnClickListener(v->{
            Intent intent = new Intent(PreviewPostActivity.this, EditPostActivity.class);
            intent.putExtra("postId", post.getId());
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onPositiveButtonClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            FirebaseDatabase.getInstance().getReference("posts").child(post.getId())
                    .setValue(null)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            FirebaseStorage.getInstance().getReference("posts")
                                    .child(post.getUserId()+"_"+post.getId())
                                    .delete();
                            Toast.makeText(PreviewPostActivity.this,"Post Deleted!",Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull  Exception e) {
                            Toast.makeText(PreviewPostActivity.this,"Failed to delete the post! "+e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        onBackPressed();

    }

    @Override
    public void onNegativeButtonClicked() {

    }

    private void loadPost(){
        binding.readPost.readpostProgressbar.setVisibility(View.VISIBLE);
        String postId = getIntent().getStringExtra("postId");
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("posts");
        databaseReference.child(postId)
                .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                post = dataSnapshot.getValue(Post.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    LocalDateTime date = LocalDateTime.parse(post.getCreatedDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                    LocalDateTime dateNow = LocalDateTime.now();
                    Duration duration = Duration.between(date, dateNow);
                    post.setHours(duration.toHours());
                }
                if (post.getTags() == null){
                    post.setTags(new ArrayList<>());
                }
                FirebaseDatabase.getInstance().getReference("users")
                        .child(post.getUserId())
                        .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                    @Override
                    public void onSuccess(DataSnapshot dataSnapshot) {
                        user = dataSnapshot.getValue(User.class);
                        Picasso.with(getApplicationContext()).load(post.getImage()).into(binding.readPost.postImage);
                        Picasso.with(getApplicationContext()).load(user.getProfpictUrl()).into(binding.readPost.profilePicture);
                        binding.readPost.titleTextview.setText(post.getTitle());
                        binding.readPost.textviewViews.setText(""+post.getViews().intValue());
                        binding.readPost.textviewHour.setText(post.getHours().intValue()+" h");
                        binding.readPost.textviewWho.setText("You");
                        binding.readPost.textviewBody.setText(post.getBody());
                        binding.readPost.chipgroupTags.removeAllViews();
                        setChipTags(post.getTags());
                        binding.readPost.readpostProgressbar.setVisibility(View.GONE);
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(PreviewPostActivity.this, "Failed to load writer of this post! "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                binding.readPost.readpostProgressbar.setVisibility(View.GONE);
                            }
                        });
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(PreviewPostActivity.this, "Failed to load post! "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        binding.readPost.readpostProgressbar.setVisibility(View.GONE);
                    }
                });
    }
    private void setChipTags(List<String> tags){
        binding.readPost.chipgroupTags.removeAllViews();
        for (String tag:
                tags) {
            @SuppressLint("ResourceType")
            Chip chip = (Chip) LayoutInflater.from(binding.getRoot().getContext()).inflate(R.layout.item_chip_readpost, null, true);
            chip.setText(tag);
            Typeface typeface = ResourcesCompat.getFont(getApplicationContext(), R.font.poppins_semibold);
            chip.setTypeface(typeface);
            binding.readPost.chipgroupTags.addView(chip);
        }
    }
}