package com.rizqi.paragraph.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.transition.Fade;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.adapter.FilterDialog;
import com.rizqi.paragraph.adapter.PopularTagsAdapter;
import com.rizqi.paragraph.adapter.SearchFilteredRecyclerViewAdapter;
import com.rizqi.paragraph.databinding.ActivitySearchExpandedBinding;
import com.rizqi.paragraph.databinding.ItemFilterDialogBinding;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.model.User;
import com.rizqi.paragraph.service.PostService;
import com.rizqi.paragraph.service.TagService;

import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class SearchExpandedActivity extends AppCompatActivity {

    private ActivitySearchExpandedBinding binding;
    private SearchFilteredRecyclerViewAdapter adapter;
    private FilterDialog filter;
    private Dialog dialog;
    private List<Post> allPost;
    private List<String> allTags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySearchExpandedBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

    @Override
    protected void onStart() {
        super.onStart();
        allPost = new ArrayList<>();
        allTags = new ArrayList<>();
        filter = new FilterDialog(ItemFilterDialogBinding.inflate(getLayoutInflater()),getApplicationContext(),new ArrayList<>());
        setupPost();
        setUpDialog();

        binding.checkboxFilter.setOnClickListener(v -> dialog.show());
        filter.binding.buttonApply.setOnClickListener(v->filterPost());
        filter.binding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search = binding.edittextSearch.getText().toString();
                List<Post> result = PostService.searchPostByKeyword(allPost,search);
                adapter.setSearchResults(result);
                filter.clearForm();
                binding.checkboxFilter.setChecked(false);
                dialog.cancel();
            }
        });
        dialog.setOnCancelListener(dialog -> {
            if (filter.isValid()){
                binding.checkboxFilter.setChecked(true);
            }else{
                binding.checkboxFilter.setChecked(false);
            }
        });
        binding.imagebuttonBack.setOnClickListener(v->finishAfterTransition());
        binding.imagebuttonGosearch.setOnClickListener(v->{
            String search = binding.edittextSearch.getText().toString();
            List<Post> result = PostService.searchPostByKeyword(allPost,search);
            adapter.setSearchResults(result);
        });
        binding.edittextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String search = binding.edittextSearch.getText().toString();
                if (search.trim().length() > 0){
                    List<Post> result = PostService.searchPostByKeyword(allPost,search);
                    adapter.setSearchResults(result);
                }else {
                    adapter.setSearchResults(allPost);
                    adapter.notifyDataSetChanged();
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAfterTransition();
    }

    private void setUpDialog() {
        dialog = new Dialog(SearchExpandedActivity.this);
        dialog.setContentView(filter.binding.getRoot());
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.80);
        int height = (int)(getResources().getDisplayMetrics().heightPixels);
        dialog.getWindow().setLayout(width, height);
        dialog.getWindow().setGravity(Gravity.RIGHT);
        dialog.getWindow().setLayout(width, height);
        dialog.setCancelable(true);
        filter.binding.autocompleteTag.setOnItemClickListener((parent, view, position, id) -> filter.setTag(parent.getItemAtPosition(position).toString()));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void filterPost(){
        if (filter.isValid()){
            String keyword = binding.edittextSearch.getText().toString();
            String who = filter.getByWho();
            String tag = filter.getTag();
            Long viewFrom = filter.getViewsFrom();
            Long viewTo = filter.getViewsTo();
            Long hourFrom = filter.getHoursFrom();
            Long hourTo = filter.getHoursTo();
            FirebaseDatabase.getInstance().getReference("users")
                    .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                    @Override
                    public void onSuccess(DataSnapshot dataSnapshot) {
                        String whoId = "";
                        for (DataSnapshot snap:
                             dataSnapshot.getChildren()) {
                            User user = snap.getValue(User.class);
                            if (user.getFullname().trim().equalsIgnoreCase(who.trim())){
                                whoId = user.getId();
                                break;
                            }
                        }
                        List<Post> result = PostService.searchAndFilter(allPost,keyword, whoId, tag,viewFrom, viewTo, hourFrom, hourTo);
                        adapter.setSearchResults(result);
                        binding.checkboxFilter.setChecked(true);
                    }
                });
        }
        else {
            binding.checkboxFilter.setChecked(false);
            adapter.setSearchResults(allPost);
        }
        dialog.dismiss();
    }

    private void setupPost(){
        FirebaseDatabase.getInstance().getReference("tags")
                .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                List<String> tagList = (List<String>) dataSnapshot.getValue();
                allTags = tagList;
                FirebaseDatabase.getInstance().getReference("posts")
                        .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                            @Override
                            public void onSuccess(DataSnapshot dataSnapshot) {
                                for (DataSnapshot snap:
                                        dataSnapshot.getChildren()) {
                                    Post post = snap.getValue(Post.class);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        LocalDateTime date = LocalDateTime.parse(post.getCreatedDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                                        LocalDateTime dateNow = LocalDateTime.now();
                                        Duration duration = Duration.between(date, dateNow);
                                        post.setHours(duration.toHours());
                                    }
                                    if (post.getTags() == null){
                                        post.setTags(new ArrayList<>());
                                    }
                                    allPost.add(post);
                                }
                                Collections.reverse(allPost);
                                String search = getIntent().getStringExtra("search");
                                binding.edittextSearch.setText(search);
                                filter.setAutoComplete(SearchExpandedActivity.this, tagList);
                                adapter = new SearchFilteredRecyclerViewAdapter(PostService.searchPostByKeyword(allPost, search));
                                binding.recyclerviewFilteredsearch.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                binding.recyclerviewFilteredsearch.setAdapter(adapter);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull @NotNull Exception e) {
                                Toast.makeText(SearchExpandedActivity.this, "Failed to load posts! "+e.getMessage(), Toast.LENGTH_SHORT);
                            }
                        });
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(SearchExpandedActivity.this, "Failed to load tags! "+e.getMessage(), Toast.LENGTH_SHORT);
                    }
                });
    }
}