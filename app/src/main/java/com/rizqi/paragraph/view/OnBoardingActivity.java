package com.rizqi.paragraph.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.rizqi.paragraph.Preferences;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.databinding.ActivityOnBoardingBinding;

public class OnBoardingActivity extends AppCompatActivity {

    private ActivityOnBoardingBinding binding;
    private static int SPLASH_TIMEOUT = 4000;
    private boolean isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityOnBoardingBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());

//        config
        isLogin = Preferences.getLoggedInStatus(getApplicationContext());
        if (isLogin == true){
            Intent intent = new Intent(getApplicationContext(), BottomNavbarActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        isLogin = Preferences.getLoggedInStatus(getApplicationContext());
        if (isLogin == true){
            Intent intent = new Intent(getApplicationContext(), BottomNavbarActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        else{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent splashIntent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(splashIntent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }, SPLASH_TIMEOUT);
        }

    }
}