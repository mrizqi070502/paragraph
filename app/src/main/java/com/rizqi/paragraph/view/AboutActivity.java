package com.rizqi.paragraph.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.rizqi.paragraph.R;
import com.rizqi.paragraph.databinding.ActivityAboutBinding;

public class AboutActivity extends AppCompatActivity {

    ActivityAboutBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAboutBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.buttonBack.setOnClickListener(v->finish());
    }
}