package com.rizqi.paragraph.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.transition.Fade;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.adapter.PopularTagsAdapter;
import com.rizqi.paragraph.databinding.FragmentSearchBinding;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.service.TagService;

import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchFragment extends Fragment {

    private FragmentSearchBinding binding;
    private FragmentManager fragmentManager;

    public SearchFragment(FragmentManager fragmentManager){
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentSearchBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        setupPost();
        binding.tabslayoutPopulartags.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewpagerPopulartags.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        binding.viewpagerPopulartags.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                binding.tabslayoutPopulartags.selectTab(binding.tabslayoutPopulartags.getTabAt(position));
            }
        });

//        shared element of search box
        Fade fade = new Fade();
        View decor = getActivity().getWindow().getDecorView();
        fade.excludeTarget(decor.findViewById(R.id.action_bar_container), true);
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        setEnterTransition(fade);
        setExitTransition(fade);
        binding.imagebuttonGosearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goSearch();
            }
        });
        binding.edittextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                goSearch();
                return false;
            }
        });
    }

    private void goSearch(){
        String search = binding.edittextSearch.getText().toString();
            Intent intent = new Intent(getContext(), SearchExpandedActivity.class);
            intent.putExtra("search",search);
            ActivityOptionsCompat compat = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), binding.relativeLayoutSearch, ViewCompat.getTransitionName(binding.relativeLayoutSearch));
            startActivity(intent, compat.toBundle());
    }

    private void setupPost(){
        FirebaseDatabase.getInstance().getReference("tags")
                .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                    @Override
                    public void onSuccess(DataSnapshot dataSnapshot) {
                        List<String> allTags = (List<String>) dataSnapshot.getValue();
                        FirebaseDatabase.getInstance().getReference("posts")
                                .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                                    @Override
                                    public void onSuccess(DataSnapshot dataSnapshot) {
                                        List<Post> allPost = new ArrayList<>();
                                        for (DataSnapshot snap:
                                                dataSnapshot.getChildren()) {
                                            Post post = snap.getValue(Post.class);
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                                LocalDateTime date = LocalDateTime.parse(post.getCreatedDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                                                LocalDateTime dateNow = LocalDateTime.now();
                                                Duration duration = Duration.between(date, dateNow);
                                                post.setHours(duration.toHours());
                                            }
                                            if (post.getTags() == null){
                                                post.setTags(new ArrayList<>());
                                            }
                                            allPost.add(post);
                                        }
                                        Map<String, List<Post>> postOfPopularTags = TagService.getPopularTag(allPost,allTags);
                                        List<String> popularTagList = TagService.getPopularString(postOfPopularTags);
                                        PopularTagsAdapter tagsAdapter = new PopularTagsAdapter(fragmentManager,getLifecycle());
                                        PopularTagsFragment fragment;
                                        for (int i = 0; i < popularTagList.size(); i++) {
                                            fragment = new PopularTagsFragment(postOfPopularTags.get(popularTagList.get(i)));
                                            tagsAdapter.addFragment(fragment, popularTagList.get(i));
                                        }
                                        binding.tabslayoutPopulartags.removeAllTabs();
                                        for (int i = 0; i < popularTagList.size(); i++) {
                                            binding.tabslayoutPopulartags.addTab(binding.tabslayoutPopulartags.newTab().setText(popularTagList.get(i)));
                                        }
                                        binding.viewpagerPopulartags.setAdapter(tagsAdapter);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull @NotNull Exception e) {
                                        Toast.makeText(getContext(), "Failed to load posts! "+e.getMessage(), Toast.LENGTH_SHORT);
                                    }
                                });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull @NotNull Exception e) {
                        Toast.makeText(getContext(), "Failed to load tags! "+e.getMessage(), Toast.LENGTH_SHORT);
                    }
                });
    }

}