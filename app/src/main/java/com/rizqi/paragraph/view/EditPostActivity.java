package com.rizqi.paragraph.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.chip.Chip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.databinding.ActivityEditPostBinding;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.model.User;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class EditPostActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 1;
    private ActivityEditPostBinding binding;
    private Post post;
    private List<String> defaultTags;
    private List<String> resultTags;
    private List<String> baseTags;
    private List<String> newTags;
    private ArrayAdapter<String> adapter;
    private Uri mImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditPostBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.createPost.cardviewBottom.setVisibility(View.GONE);
        post = new Post();
        defaultTags = new ArrayList<>();
        resultTags = new ArrayList<>();
        baseTags = new ArrayList<>();
        newTags = new ArrayList<>();
    }

    @Override
    protected void onStart() {
        super.onStart();
        binding.createPost.buttonBack.setOnClickListener(v -> onBackPressed());
        loadTags();
        loadPost();
        adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.autocomplete_tags, defaultTags);
        binding.createPost.autocompleteTag.setAdapter(adapter);
        binding.createPost.buttonNewTag.setOnClickListener(v->{
            createChip(binding.createPost.autocompleteTag.getText().toString());
        });
        binding.createPost.autocompleteTag.setOnItemClickListener((parent, view, position, id) -> createChip(adapter.getItem(position)));
        binding.createPost.buttonChooseimage.setOnClickListener(v->{
            openImageDirectory();
        });
        binding.createPost.compattextviewRemoveimage.setOnClickListener(v->{
            Picasso.with(EditPostActivity.this).load(post.getImage()).into(binding.createPost.imageviewShowimage);
            mImageUri = null;
        });
        binding.linearlayoutDiscard.setOnClickListener(v->onBackPressed());
        binding.buttonDiscard.setOnClickListener(v->onBackPressed());
        binding.buttonSave.setOnClickListener(v->savePost());
        binding.linearlayoutSave.setOnClickListener(v->savePost());
        binding.createPost.buttonBack.setOnClickListener(v->onBackPressed());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            mImageUri = data.getData();
            Picasso.with(EditPostActivity.this).load(mImageUri).into(binding.createPost.imageviewShowimage);
        }
    }

    private void savePost(){
        binding.createPost.progressbarUploadimage.setVisibility(View.VISIBLE);
        if (mImageUri != null){
            String userId = post.getUserId();
            String imageId = userId+"_"+post.getId();
            StorageReference reference = FirebaseStorage.getInstance().getReference("posts").child(imageId);
            reference.delete()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            FirebaseStorage.getInstance().getReference("posts").child(imageId).putFile(mImageUri)
                                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                            reference.getDownloadUrl()
                                                    .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                        @Override
                                                        public void onSuccess(Uri uri) {
                                                            String imageUrl = uri.toString();
                                                            String title = binding.createPost.edittextTitle.getText().toString();
                                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                                resultTags = resultTags.stream().distinct().collect(Collectors.toList());
                                                            }
                                                            String body = binding.createPost.edittextBody.getText().toString();
                                                            String createdDate = post.getCreatedDate();
                                                            Post editPost = new Post(post.getId(),title, imageUrl, resultTags, body, userId, post.getViews(), post.getHours(), createdDate);
                                                            FirebaseDatabase.getInstance().getReference("posts").child(post.getId())
                                                                    .setValue(editPost)
                                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                        @Override
                                                                        public void onSuccess(Void unused) {
                                                                            DatabaseReference tagsReference = FirebaseDatabase.getInstance().getReference("tags");
                                                                            tagsReference.get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                                                                                @Override
                                                                                public void onSuccess(DataSnapshot dataSnapshot) {
                                                                                    int size = ((List<String>)dataSnapshot.getValue()).size();
                                                                                    for (String s:
                                                                                            newTags) {
                                                                                        tagsReference.child(String.valueOf(size)).setValue(s);
                                                                                        size++;
                                                                                        Toast.makeText(EditPostActivity.this, "Saved successful", Toast.LENGTH_SHORT).show();
                                                                                        binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
                                                                                        finish();
                                                                                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                                                                                    }
                                                                                }
                                                                            })
                                                                                    .addOnFailureListener(new OnFailureListener() {
                                                                                        @Override
                                                                                        public void onFailure(@NonNull Exception e) {
                                                                                            Toast.makeText(EditPostActivity.this, "saved successful", Toast.LENGTH_SHORT).show();
                                                                                            binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
                                                                                            finish();
                                                                                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                                                                                        }
                                                                                    });
                                                                        }
                                                                    })
                                                                    .addOnFailureListener(new OnFailureListener() {
                                                                        @Override
                                                                        public void onFailure(@NonNull Exception e) {
                                                                            Toast.makeText(EditPostActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                                            binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
                                                                        }
                                                                    });
                                                        }
                                                    })
                                                    .addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Toast.makeText(EditPostActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                            binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
                                                        }
                                                    });
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(EditPostActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                            binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(EditPostActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
                        }
                    });
        }else {
            String userId = post.getUserId();
            String title = binding.createPost.edittextTitle.getText().toString();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                resultTags = resultTags.stream().distinct().collect(Collectors.toList());
            }
            String body = binding.createPost.edittextBody.getText().toString();
            String createdDate = post.getCreatedDate();
            Post editPost = new Post(post.getId(),title, post.getImage(), resultTags, body, userId, post.getViews(), post.getHours(), createdDate);
            FirebaseDatabase.getInstance().getReference("posts").child(post.getId())
                    .setValue(editPost)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            DatabaseReference tagsReference = FirebaseDatabase.getInstance().getReference("tags");
                            tagsReference.get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                                @Override
                                public void onSuccess(DataSnapshot dataSnapshot) {
                                    int size = ((List<String>)dataSnapshot.getValue()).size();
                                    for (String s:
                                            newTags) {
                                        tagsReference.child(String.valueOf(size)).setValue(s);
                                        size++;
                                        Toast.makeText(EditPostActivity.this, "Saved successful", Toast.LENGTH_SHORT).show();
                                        binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
                                        onBackPressed();
                                    }
                                }
                            })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(EditPostActivity.this, "Saved successful", Toast.LENGTH_SHORT).show();
                                            binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
                                            onBackPressed();
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(EditPostActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
                        }
                    });
            binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
        }
        binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
    }

    private void loadPost(){
        binding.createPost.progressbarUploadimage.setVisibility(View.VISIBLE);
        String postId = getIntent().getStringExtra("postId");
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("posts");
        databaseReference.child(postId)
                .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                    @Override
                    public void onSuccess(DataSnapshot dataSnapshot) {
                        post = dataSnapshot.getValue(Post.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            LocalDateTime date = LocalDateTime.parse(post.getCreatedDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                            LocalDateTime dateNow = LocalDateTime.now();
                            Duration duration = Duration.between(date, dateNow);
                            post.setHours(duration.toHours());
                        }
                        if (post.getTags() == null){
                            post.setTags(new ArrayList<>());
                        }
                        binding.createPost.edittextTitle.setText(post.getTitle());
                        binding.createPost.tagchipLinearlayout.removeAllViews();
                        for (String s:
                             post.getTags()) {
                            createChip(s);
                        }
                        binding.createPost.edittextBody.setText(post.getBody());
                        if (mImageUri == null){
                            Picasso.with(EditPostActivity.this).load(post.getImage()).into(binding.createPost.imageviewShowimage);
                        }else {
                            Picasso.with(EditPostActivity.this).load(mImageUri).into(binding.createPost.imageviewShowimage);
                        }
                        binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(EditPostActivity.this, "Failed to load post! "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
                    }
                });
        binding.createPost.progressbarUploadimage.setVisibility(View.GONE);
    }

    private void loadTags(){
        FirebaseDatabase.getInstance()
                .getReference("tags")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        for (DataSnapshot snapshot:
                                task.getResult().getChildren()) {
                            defaultTags.add(snapshot.getValue(String.class));
                            baseTags.add(snapshot.getValue(String.class));
                            adapter.notifyDataSetChanged();
                        }
                    }else {
                        System.out.println("Failed to load tags");
                    }
                });
    }

    private void createChip(String text){
        if (!resultTags.contains(text)){
            Chip chip = new Chip(EditPostActivity.this);
            chip.setText(text);
            chip.setChipMinHeight(90);
            chip.setCloseIconVisible(true);
            chip.setTextColor(getResources().getColor(R.color.grey_1));
            chip.setChipBackgroundColor(ColorStateList.valueOf(getResources().getColor(R.color.grey_5)));
            Typeface typeface = ResourcesCompat.getFont(getApplicationContext(), R.font.poppins);
            chip.setTypeface(typeface);
            chip.setTextSize(10);
            chip.setTextEndPadding(0);
            chip.setCloseIconStartPadding(0);
            int colorInt = getResources().getColor(R.color.grey_1);
            chip.setCloseIconTint(ColorStateList.valueOf(colorInt));
            chip.setOnCloseIconClickListener(v -> {
                binding.createPost.tagchipLinearlayout.removeView(v);
                defaultTags.add(text);
                newTags.remove(text);
                resultTags.remove(text);
                System.out.println("DEFAULT : "+ Arrays.toString(defaultTags.toArray()));
                System.out.println("NEW : "+Arrays.toString(newTags.toArray()));
                System.out.println("RESULT : "+Arrays.toString(resultTags.toArray()));
                adapter.add(text);
                adapter.notifyDataSetChanged();
            });
            if (!baseTags.contains(text)){
                newTags.add(text);
            }
            binding.createPost.tagchipLinearlayout.addView(chip);
            defaultTags.remove(text);
            resultTags.add(text);
            System.out.println("DEFAULT : "+Arrays.toString(defaultTags.toArray()));
            System.out.println("NEW : "+Arrays.toString(newTags.toArray()));
            System.out.println("RESULT : "+Arrays.toString(resultTags.toArray()));
            adapter.remove(text);
            adapter.notifyDataSetChanged();
        }
        binding.createPost.autocompleteTag.setText("");
    }
    private void openImageDirectory(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}