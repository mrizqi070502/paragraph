package com.rizqi.paragraph.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.adapter.AllPostRecycleAdapter;
import com.rizqi.paragraph.adapter.FilterDialog;
import com.rizqi.paragraph.databinding.ActivityOrganizePostBinding;
import com.rizqi.paragraph.databinding.ItemFilterDialogBinding;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.service.PostService;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class OrganizePostActivity extends AppCompatActivity {

    private ActivityOrganizePostBinding binding;
    private List<Post> allYourPostList;
    private List<String> defaultTags;
    private AllPostRecycleAdapter adapter;
    private FilterDialog filter;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOrganizePostBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.cardviewCreatepost.setOnClickListener(v->{
            Intent intent = new Intent(OrganizePostActivity.this, CreatePostActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
        binding.buttonBack.setOnClickListener(v->onBackPressed());
        filter = new FilterDialog(ItemFilterDialogBinding.inflate(getLayoutInflater()),getApplicationContext(),defaultTags);
        setUpFilterDialog();

    }

    @Override
    protected void onStart() {
        super.onStart();
        allYourPostList = new ArrayList<>();
        defaultTags = new ArrayList<>();
        setAllYourPost();
        loadTags();
        adapter = new AllPostRecycleAdapter(allYourPostList);
        binding.recyclerviewAllyourpost.setLayoutManager(new LinearLayoutManager(OrganizePostActivity.this));
        binding.recyclerviewAllyourpost.setAdapter(adapter);
        binding.checkboxFilter.setOnClickListener(v -> dialog.show());
        filter.binding.buttonApply.setOnClickListener(v->filterPost());
        filter.binding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter.clearForm();
                adapter.setAllYourPost(allYourPostList);
                binding.checkboxFilter.setChecked(false);
                dialog.cancel();
            }
        });
        dialog.setOnCancelListener(dialog -> {
            if (filter.isValid()){
                binding.checkboxFilter.setChecked(true);
            }else{
                binding.checkboxFilter.setChecked(false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void filterPost(){
        if (filter.isValid()){
            String tag = filter.getTag();
            Long viewFrom = filter.getViewsFrom();
            Long viewTo = filter.getViewsTo();
            Long hourFrom = filter.getHoursFrom();
            Long hourTo = filter.getHoursTo();
            List<Post> result = PostService.filterPosts(allYourPostList, tag,viewFrom, viewTo, hourFrom, hourTo);
            adapter.setAllYourPost(result);
            binding.checkboxFilter.setChecked(true);
        }
        else {
            binding.checkboxFilter.setChecked(false);
            adapter.setAllYourPost(allYourPostList);
        }
        dialog.dismiss();
    }

    private void setUpFilterDialog() {
        dialog = new Dialog(OrganizePostActivity.this);
        dialog.setContentView(filter.binding.getRoot());
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.80);
        int height = (int)(getResources().getDisplayMetrics().heightPixels);
        dialog.getWindow().setLayout(width, height);
        dialog.getWindow().setGravity(Gravity.RIGHT);
        dialog.getWindow().setLayout(width, height);
        dialog.setCancelable(true);
        filter.binding.inputlayoutWho.setVisibility(View.GONE);
        filter.binding.textviewWho.setVisibility(View.GONE);
        filter.binding.autocompleteTag.setOnItemClickListener((parent, view, position, id) -> filter.setTag(parent.getItemAtPosition(position).toString()));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void loadTags(){
        FirebaseDatabase.getInstance()
                .getReference("tags")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        for (DataSnapshot snapshot:
                                task.getResult().getChildren()) {
                            defaultTags.add(snapshot.getValue(String.class));
                            filter.setTagList(defaultTags, OrganizePostActivity.this);
                        }
                    }else {
                        System.out.println("Failed to load tags");
                    }
                });
    }

    private void setAllYourPost(){
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Query query = FirebaseDatabase.getInstance().getReference("posts")
                .orderByChild("userId")
                .equalTo(userId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Post post = dataSnapshot.getValue(Post.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        LocalDateTime date = LocalDateTime.parse(post.getCreatedDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                        LocalDateTime dateNow = LocalDateTime.now();
                        Duration duration = Duration.between(date, dateNow);
                        post.setHours(duration.toHours());
                    }
                    if (post.getTags() == null){
                        post.setTags(new ArrayList<>());
                    }
                    allYourPostList.add(post);
                    adapter.notifyDataSetChanged();
                    binding.textviewAllyourpost.setText("All your post("+allYourPostList.size()+")");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}