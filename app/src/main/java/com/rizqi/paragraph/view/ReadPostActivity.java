package com.rizqi.paragraph.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.chip.Chip;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rizqi.paragraph.R;
import com.rizqi.paragraph.databinding.ActivityReadPostBinding;
import com.rizqi.paragraph.model.Post;
import com.rizqi.paragraph.model.User;
import com.squareup.picasso.Picasso;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ReadPostActivity extends AppCompatActivity {

    ActivityReadPostBinding binding;
    Post post;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityReadPostBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

    @Override
    protected void onStart() {
        super.onStart();
        post = new Post();
        user = new User();
        binding.buttonBack.setOnClickListener(v -> onBackPressed());
        loadPost();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void loadPost(){
        binding.readpostProgressbar.setVisibility(View.VISIBLE);
        String postId = getIntent().getStringExtra("postId");
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("posts");
        databaseReference.child(postId)
                .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                post = dataSnapshot.getValue(Post.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    LocalDateTime date = LocalDateTime.parse(post.getCreatedDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                    LocalDateTime dateNow = LocalDateTime.now();
                    Duration duration = Duration.between(date, dateNow);
                    post.setHours(duration.toHours());
                }
                if (post.getTags() == null){
                    post.setTags(new ArrayList<>());
                }
                post.setViews(post.getViews()+1);
                FirebaseDatabase.getInstance().getReference("users")
                        .child(post.getUserId())
                        .get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
                            @Override
                            public void onSuccess(DataSnapshot dataSnapshot) {
                                user = dataSnapshot.getValue(User.class);
                                Picasso.with(getApplicationContext()).load(post.getImage()).into(binding.postImage);
                                Picasso.with(getApplicationContext()).load(user.getProfpictUrl()).into(binding.profilePicture);
                                binding.titleTextview.setText(post.getTitle());
                                binding.textviewViews.setText(""+post.getViews().intValue());
                                binding.textviewHour.setText(post.getHours().intValue()+" h");
                                binding.textviewWho.setText(formatName(user.getFullname()));
                                binding.textviewBody.setText(post.getBody());
                                binding.chipgroupTags.removeAllViews();
                                setChipTags(post.getTags());
                                binding.readpostProgressbar.setVisibility(View.GONE);
                                DatabaseReference reference = FirebaseDatabase.getInstance().getReference("posts");
                                reference.child(postId).child("views").setValue(post.getViews());
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(ReadPostActivity.this, "Failed to load writer of this post! "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                binding.readpostProgressbar.setVisibility(View.GONE);
                            }
                        });
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(ReadPostActivity.this, "Failed to load post! "+e.getMessage(), Toast.LENGTH_SHORT).show();
                binding.readpostProgressbar.setVisibility(View.GONE);
            }
        });
    }

    private String formatName(String fullname){
        String temp1 = user.getFullname().replace("Muhammad","M")
                .replace("Mohammad","M")
                .replace("_"," ");
        String[] split = temp1.split(" ");
        String finalName = "";
        int i = 0;
        if (split[0].equals("M")){
            finalName = "M "+split[1];
            i = 2;
        }else {
            finalName = split[0];
            i = 1;
        }
        for (; i < split.length; i++) {
            finalName += " "+split[i].toUpperCase().charAt(0);
        }
        return finalName;
    }

    private void setChipTags(List<String> tags){
        binding.chipgroupTags.removeAllViews();
        for (String tag:
                tags) {
            @SuppressLint("ResourceType")
            Chip chip = (Chip) LayoutInflater.from(binding.getRoot().getContext()).inflate(R.layout.item_chip_readpost, null, true);
            chip.setText(tag);
            Typeface typeface = ResourcesCompat.getFont(getApplicationContext(), R.font.poppins_semibold);
            chip.setTypeface(typeface);
            binding.chipgroupTags.addView(chip);
        }
    }

}