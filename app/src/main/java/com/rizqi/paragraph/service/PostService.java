package com.rizqi.paragraph.service;

import android.os.Build;

import com.rizqi.paragraph.model.Post;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PostService {

    public static List<Post> filterPosts(List<Post> postList, String tag, Long viewFrom, Long viewTo, Long hourFrom, Long hourTo){
        List<Post> result = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = postList.stream()
                    .filter(post -> {
                        if (tag.trim().length() < 1){
                            return true;
                        }
                        boolean isFind = false;
                        for (String t:
                                post.getTags().stream().distinct().collect(Collectors.toList())) {
                            if (t.toLowerCase().contains(tag.toLowerCase())){
                                isFind = true;
                                break;
                            }
                        }
                        return isFind;
                    })
                    .filter(post -> {
                        if (viewFrom >= 0 && viewTo >= 0){
                            return ((post.getViews().longValue() >= viewFrom.longValue()) && (post.getViews().longValue() <= viewTo.longValue())) ? true : false;
                        }
                        return true;

                    })
                    .filter(post -> {
                        if (hourFrom >= 0 && hourTo >= 0){
                            return ((post.getHours().longValue() >= hourFrom.longValue())) && ((post.getHours().longValue() <= hourTo.longValue())) ? true : false;
                        }
                        return true;

                    })
                    .collect(Collectors.toList());
        }
        return result;
    }

    public static List<Post> getTrendingPosts(List<Post> postList){
        List<Post> trendPost = new ArrayList<>();
        Comparator<Post> comparator = new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                Double speed1 = (double)o1.getViews()/(double)(o1.getHours()==0? 1: o1.getHours());
                Double speed2 = (double)o2.getViews()/(double)(o2.getHours()==0? 1: o2.getHours());
                return speed1 > speed2 ? -1 : 1;
            }
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            trendPost = postList.stream()
                    .sorted(comparator)
                    .limit(3)
                    .collect(Collectors.toList());
        }
        return  trendPost;
    }

    public static List<Post> getNewPosts(List<Post> postList){
        List<Post> newPosts = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            newPosts = postList.stream()
                    .sorted((o1, o2) -> o1.getHours().longValue() < o2.getHours().longValue() ? -1 : 1)
                    .collect(Collectors.toList());
        }
        return newPosts;
    }

    public static List<Post> searchPostByKeyword(List<Post> postList, String keyword){
        List<Post> result = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result =  postList.stream()
                    .filter(post -> post.getTitle().toLowerCase().contains(keyword.toLowerCase()) ? true : false)
                    .collect(Collectors.toList());
        }
        return result;
    }

    public static List<Post> searchAndFilter(List<Post> postList, String keyword, String whoId, String tag, Long viewFrom, Long viewTo, Long hourFrom, Long hourTo){
        List<Post> result = searchPostByKeyword(postList,keyword);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = result.stream()
                    .filter(post -> {
                        if (whoId.trim().length() < 1){
                            return true;
                        }
                        return post.getUserId().equals(whoId) ? true : false;
                    })
                    .filter(post -> {
                        if (tag.trim().length() < 1){
                            return true;
                        }
                        boolean isFind = false;
                        for (String t:
                                post.getTags().stream().distinct().collect(Collectors.toList())) {
                            if (t.toLowerCase().contains(tag.toLowerCase())){
                                isFind = true;
                                break;
                            }
                        }
                        return isFind;
                    })
                    .filter(post -> {
                        if (viewFrom >= 0L && viewTo >= 0L){
                            return ((post.getViews().longValue() >= viewFrom.longValue()) && (post.getViews().longValue() <= viewTo.longValue())) ? true : false;
                        }
                        return true;

                    })
                    .filter(post -> {
                        if (hourFrom >= 0L && hourTo >= 0L){
                            return ((post.getHours().longValue() >= hourFrom.longValue())) && ((post.getHours().longValue() <= hourTo.longValue())) ? true : false;
                        }
                        return true;

                    })
                    .collect(Collectors.toList());
        }
        return result;
    }

}
