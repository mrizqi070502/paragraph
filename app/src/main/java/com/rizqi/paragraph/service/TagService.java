package com.rizqi.paragraph.service;

import com.rizqi.paragraph.model.Post;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TagService {

    public static Map<String, List<Post>> getPopularTag(List<Post> allPost, List<String> tags){
        Map<String, List<Post>> temp = new HashMap<>();
        for (String tag:
                tags) {
            List<Post> p = new ArrayList<>();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                p = allPost.stream().filter(post -> post.getTags().contains(tag) ? true : false).collect(Collectors.toList());
            }
            temp.put(tag,p);
        }
        List<Map.Entry<String, List<Post>>> list = new LinkedList<Map.Entry<String, List<Post>>>(temp.entrySet());
        Collections.sort(list, (o1, o2) -> o1.getValue().size() > o2.getValue().size() ? -1 : 1);
        Map<String, List<Post>> sortedMap = new LinkedHashMap<String, List<Post>>();
        for (int i = 0; i < 5; i++)
        {
            Map.Entry<String, List<Post>> entry = list.get(i);
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    public static List<String> getPopularString(Map<String, List<Post>> popular){
        List<String> tagsString = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            tagsString = popular.keySet().stream().collect(Collectors.toList());
        }
        return tagsString;
    }
}
